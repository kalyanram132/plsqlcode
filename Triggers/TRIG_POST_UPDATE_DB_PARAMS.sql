--------------------------------------------------------
--  DDL for Trigger TRIG_POST_UPDATE_DB_PARAMS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "DEMANTRA"."TRIG_POST_UPDATE_DB_PARAMS" -- $Revision: 1.9 $
AFTER UPDATE OR INSERT
OF pval
ON db_params

DECLARE
   vi_exists      INTEGER;
   job            NUMBER;

BEGIN

   DBEX('Starting Trigger','TRIG_POST_UPDATE_DB_PARAMS','A');

   /*******************************************************************************/
   /* Only create a job for BUILD_PRE_LOGON if there not already one in the queue */
   /*******************************************************************************/
   SELECT COUNT(1)  INTO vi_exists  FROM   user_jobs    WHERE  UPPER(what) LIKE '%BUILD_PRE_LOGON%';

   IF vi_exists = 0 THEN

      DBEX('dbms_job.submit','TRIG_POST_UPDATE_DB_PARAMS','A');

	  dbms_job.submit(job, 'build_pre_logon;');

   END IF;

END;



/
ALTER TRIGGER "DEMANTRA"."TRIG_POST_UPDATE_DB_PARAMS" ENABLE;

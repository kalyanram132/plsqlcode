--------------------------------------------------------
--  DDL for Trigger APPTRIG_GROUP_ATTRIBUTES
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "DEMANTRA"."APPTRIG_GROUP_ATTRIBUTES" 
   AFTER DELETE OR INSERT OR UPDATE of group_column
   ON group_attributes
DECLARE
   PRAGMA AUTONOMOUS_TRANSACTION;
/******************************************************************************
   NAME:      apptrig_group_attributes
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        19/01/2012  Luke Pocock      Re-create Global Temp tables for Duplicate Procedures
                                           when creating level Attributes

   NOTES:

******************************************************************************/
BEGIN
   dynamic_ddl ('drop table dup_p');
   dynamic_ddl
      ('CREATE GLOBAL TEMPORARY TABLE DUP_P
      ON COMMIT DELETE ROWS
      NOCACHE
      as select * from promotion where 1=2'
      );
   dynamic_ddl ('drop table dup_pd');
   dynamic_ddl
      ('CREATE GLOBAL TEMPORARY TABLE DUP_PD
      ON COMMIT DELETE ROWS
      NOCACHE
      as select * from promotion_data where 1=2'
      );
   dynamic_ddl ('drop table dup_pm');
   dynamic_ddl
      ('CREATE GLOBAL TEMPORARY TABLE DUP_PM
      ON COMMIT DELETE ROWS
      NOCACHE
      as select * from promotion_matrix where 1=2'
      );
   dynamic_ddl ('drop table DUP_PDT');
   dynamic_ddl
      ('CREATE GLOBAL TEMPORARY TABLE DUP_PDT
      ON COMMIT DELETE ROWS
      NOCACHE
      as select * from promotion_dates where 1=2'
      );
      dynamic_ddl ('drop table DUP_CLAIM');
      dynamic_ddl
      ('CREATE GLOBAL TEMPORARY TABLE  dup_claim
      ON COMMIT DELETE ROWS
      NOCACHE
      as select * from settlement where 1=2'
      );
END;




/
ALTER TRIGGER "DEMANTRA"."APPTRIG_GROUP_ATTRIBUTES" ENABLE;

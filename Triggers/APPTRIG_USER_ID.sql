--------------------------------------------------------
--  DDL for Trigger APPTRIG_USER_ID
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "DEMANTRA"."APPTRIG_USER_ID" 
   AFTER DELETE OR INSERT or UPDATE of USER_NAME
   ON USER_ID
   REFERENCING NEW AS NEW OLD AS OLD
   FOR EACH ROW   
DECLARE
--   PRAGMA AUTONOMOUS_TRANSACTION;
/******************************************************************************
   NAME:       APPTRIG_USER_ID
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        22/11/2013  Mamta Jangra     Re-create rr_user_id table when a insert or Update on USER_ID 
                                           

   NOTES:

******************************************************************************/

v_count NUMBER;

BEGIN


IF DELETING THEN 
  DELETE FROM rr_user_id WHERE user_id = :OLD.user_id;
ELSIF INSERTING THEN 

    IF (LOWER (:NEW.USER_NAME) LIKE '%inactive%' )
    THEN
      INSERT INTO rr_user_id(user_name, user_id, is_locked)
           VALUES (:new.user_name, :new.user_id, :new.is_locked);
    END IF;
    
ELSIF UPDATING THEN 
   
   IF (LOWER (:NEW.USER_NAME) LIKE '%inactive%' )
    THEN
      
      SELECT COUNT(1) INTO v_count 
      FROM rr_user_id 
      WHERE user_id = :NEW.user_id;
      
      IF v_count = 0 THEN 
      
      INSERT INTO rr_user_id(user_name, user_id, is_locked)
           VALUES (:new.user_name, :new.user_id, :new.is_locked);
           
      END IF;          
      
    ELSE 
      DELETE FROM rr_user_id
            WHERE LOWER (user_name) = LOWER (:NEW.USER_NAME);              
   END IF;
END IF;   
--COMMIT;

END;




/
ALTER TRIGGER "DEMANTRA"."APPTRIG_USER_ID" ENABLE;

--------------------------------------------------------
--  DDL for Trigger APPTRIG_PROMO_NAME
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "DEMANTRA"."APPTRIG_PROMO_NAME" 
   BEFORE INSERT
   ON DEMANTRA.PROMOTION_MATRIX    REFERENCING NEW AS inserted_promotion OLD AS OLD
   FOR EACH ROW
BEGIN
   --Change this to update the correct column
   UPDATE promotion p
      SET promotion_code = promotion_id,
          promotion_desc =                            --promotion_id||' : NEW'
             NVL ((SELECT    TO_CHAR (from_date, 'yyyy-mm-dd')
                          || ' : '
                          || promotion_id
                          || ' : NEW'
                     FROM promotion_dates pt
                    WHERE pt.promotion_id = p.promotion_id),
                  promotion_desc
                 ),
          last_update_date = SYSDATE
    WHERE promotion_id = :inserted_promotion.promotion_id;
    
END;



/
ALTER TRIGGER "DEMANTRA"."APPTRIG_PROMO_NAME" ENABLE;

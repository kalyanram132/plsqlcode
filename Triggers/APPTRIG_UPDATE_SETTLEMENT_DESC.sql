--------------------------------------------------------
--  DDL for Trigger APPTRIG_UPDATE_SETTLEMENT_DESC
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "DEMANTRA"."APPTRIG_UPDATE_SETTLEMENT_DESC" 
before insert or update of settlement_desc on settlement
for each row
declare
new_claim_desc varchar(200);
new_claim_par_id number;
is_exists number;

/*
Created By: Luke Pocock

Date: 01/06/2012

*/

begin

   BEGIN

   new_claim_desc := :new.settlement_desc;

   SELECT COUNT (*)
     INTO is_exists
     FROM claim_parent
    WHERE claim_parent_desc =  new_claim_desc; 

   IF is_exists = 0
   THEN
      INSERT INTO claim_parent
           VALUES (claim_parent_seq.NEXTVAL, claim_parent_seq.NEXTVAL,
                   new_claim_desc, SYSDATE, 1, claim_parent_seq.NEXTVAL || ':' ||  new_claim_desc
                   , 1, 0);

   END IF;
   
    SELECT min(claim_parent_id)
     INTO new_claim_par_id
     FROM claim_parent
    WHERE claim_parent_desc = new_claim_desc; 

   :new.claim_parent_id := new_claim_par_id;

   EXCEPTION
   WHEN NO_DATA_FOUND THEN
      NULL;
   END;

end;
/
ALTER TRIGGER "DEMANTRA"."APPTRIG_UPDATE_SETTLEMENT_DESC" ENABLE;

--------------------------------------------------------
--  DDL for Trigger APPTRIG_INSERT_PROMOTION
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "DEMANTRA"."APPTRIG_INSERT_PROMOTION" 
BEFORE INSERT
ON DEMANTRA.PROMOTION_DATA REFERENCING NEW AS inserted_promotion OLD AS OLD
FOR EACH ROW
DECLARE

/******************************************************************************
   NAME:       APPTRIG_INSERT_PROMOTION
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        03/20/2008  Richard Burton   1. Created this trigger.
   2.0        08/05/2012  Luke Pocock      End User Base Move into Incrementals
   2.1        12/10/2012  Richard Burton   Sales % Oride series for baseline

   NOTES:

******************************************************************************/

v_list_price_p            NUMBER;
v_cust_list_price_p       NUMBER;
v_tot_efficiencies_p      NUMBER;
v_market_price_p          NUMBER;
v_frieght_deduction_p     NUMBER;
v_bnps_p                  NUMBER;
v_bbds_p                  NUMBER;
v_rebate_p                NUMBER;
v_cogs_p                  NUMBER;
v_distribution_p          NUMBER;
v_spoils_p                NUMBER;
v_def_efficiencies_rate_p NUMBER;
v_bbds_deferred_p         NUMBER;
v_list_sales_a            NUMBER;
v_tot_efficiencies_a      NUMBER;
v_price_adjust_a          NUMBER;
v_gross_sales_a           NUMBER;
v_base_non_per_spnd_a     NUMBER;
v_ttl_base_bus_dev_spnd_a NUMBER;
v_ttl_promo_trde_spnd_a   NUMBER;
v_total_trad_a            NUMBER;
v_net_sales_a             NUMBER;
v_cogs_a                  NUMBER;
v_tot_distribution_a      NUMBER;
v_spoils_a                NUMBER;
v_non_work_co_mark_a      NUMBER;
v_cust_contribution_a     NUMBER;
v_evt_vol_act             NUMBER;
v_incr_evt_vol_act        NUMBER;
v_volume_base_ttl         NUMBER;
v_units                   NUMBER;
v_litres                  NUMBER;
v_ed_price                NUMBER;
v_rr_roi_effeciencies     NUMBER;
V_RR_ROI_REBATE           NUMBER;
v_vol                     NUMBER;
V_FRANCH_REIMB_P          NUMBER;
fore_column               VARCHAR2(30);
v_ms_cd_not_ms_promo      NUMBER;
v_ms_vol_not_ms_promo     NUMBER;   

BEGIN

 /*

   Created By: Richard Burton
   Date: 18/05/2009 - For Simplot Project
   TPM Demand  Spend Project
   Purpose: Triggered when a row is being inserted to promotion data.
            the Trigger is updating the new inserted row with the value of Sales Baseline Fcst from sales_data.

   */      
      
      IF :inserted_promotion.is_self <> 1 
      THEN       
          :inserted_promotion.activity_type_id := :inserted_promotion.promotion_type_id; 
          RETURN;      
      END IF;
      
      SELECT get_fore_col(0, 1) 
      INTO fore_column
      FROM DUAL;
      
      EXECUTE IMMEDIATE 'SELECT 
             DECODE( pt.rr_margin_support , 1 , 0, CASE WHEN 1 = 1 OR sd.sales_date >= TO_DATE('''|| TO_CHAR(NEXT_DAY(TRUNC(SYSDATE, 'DD'), 'MONDAY') - 7, 'MM/DD/YYYY') ||''', ''MM/DD/YYYY'') 
                                                                + CASE
                                                                      WHEN mdp.allocation_wk6 > 0 THEN 42
                                                                      WHEN mdp.allocation_wk5 > 0 THEN 35
                                                                      WHEN mdp.allocation_wk4 > 0 THEN 28
                                                                      WHEN mdp.allocation_wk3 > 0 THEN 21
                                                                      WHEN mdp.allocation_wk2 > 0 THEN 14
                                                                      WHEN mdp.allocation_wk1 > 0 THEN 7
                                                                      ELSE 0
                                                                   END
                  THEN CASE WHEN pt.rr_sync_base_vol = 1 AND ps.rr_sync_base_vol = 1 
                        THEN NVL(sd.manual_stat, NVL(sd.sim_val_182, NVL(sd.' || fore_column || '*1, sd.npi_forecast))) *(1.00 + NVL(sd.manual_fact,0))
                        ELSE 0 
                        END 
                  ELSE 0
             END) volume_base_ttl,
              DECODE( pt.rr_ms_exfact_pos , 0 , 0, 
              1, CASE WHEN NVL(sd.sdata5 ,0) + NVL(sd.sdata6 ,0)  <> 0 THEN NVL(sd.sdata5 ,0) + NVL(sd.sdata6 ,0) 
                 ELSE (NVL(sd.manual_stat, NVL(sd.sim_val_182,NVL(sd.' || fore_column || ', sd.npi_forecast))) * (1.00+NVL(sd.manual_fact,0)) + nvl(sd.sd_uplift,0)) END,
              2, CASE WHEN NVL(sd.actual_quantity, 0) <> 0 THEN NVL(sd.actual_quantity, 0) 
                  ELSE DECODE(NVL(sd.demand_type, 0),1,(NVL(sd.exfact_base,0)* (nvl(sd.rr_invest_or,nvl(sd.rr_invest_p,1))) + nvl(sd.final_pos_fcst,0)),
                  NVL(sd.unexfact_or,(NVL(sd.manual_stat, NVL(sd.sim_val_182,NVL(sd.' || fore_column || ', sd.npi_forecast)))*
                  (1.00+NVL(sd.manual_fact,0)) + nvl(sd.sd_uplift,0)))) END ) vol,
             DECODE( pt.rr_margin_support , 1 , 0, CASE WHEN latt6.rr_accrual_adjust = 0 AND NVL(pt.rr_accrual_adjust, 0) = 0 AND mdp.dem_stream = 0
                  THEN sd.actual_quantity
                  WHEN latt6.rr_accrual_adjust = 0 AND NVL(pt.rr_accrual_adjust, 0) = 0 AND mdp.dem_stream = 1
                  THEN sd.sdata5
                  ELSE NULL
             END) evt_vol_act,
             DECODE( pt.rr_margin_support , 1 , 0, CASE WHEN latt6.rr_accrual_adjust = 0 AND NVL(pt.rr_accrual_adjust, 0) = 0 AND mdp.dem_stream = 0
                  THEN NULL
                  WHEN latt6.rr_accrual_adjust = 0 AND NVL(pt.rr_accrual_adjust, 0) = 0 AND mdp.dem_stream = 1
                  THEN sd.sdata6
                  ELSE NULL
             END) incr_evt_vol_act,
              sd.ebspricelist109 list_price_p,
             sd.ebspricelist110 cust_list_price_p, --war_price_p,
             sd.ebspricelist111 tot_efficiencies_p, --trad_allowance_p,
             sd.ebspricelist112 price_adjust_a, --trad_allowance_amt,
             sd.ebspricelist113 base_non_per_spnd_a, --temp_price_adj_amt,
             sd.ebspricelist114 market_price_p, --tier_disc_p,
             sd.ebspricelist117 ttl_base_bus_dev_spnd_a, --trad_terms_fix_amt,
             sd.ebspricelist118 frieght_deduction_p, --trad_terms_fix_perc,
             sd.ebspricelist120 bnps_p, --reb_scale_only_p,
             sd.ebspricelist122 ttl_promo_trde_spnd_a, --trad_terms_var_amt,
             sd.ff_input1 bbds_p , --trad_terms_var_p,
             sd.cust_input3 total_trad_a,  --trade_promo_amt,
             sd.mkt_acc3 rebate_p, --trade_promo_p,
             sd.mkt_acc1 franch_reimb_p, --franch_reimb,
             sd.ebspricelist126 cogs_p, ---cogs_p
             i.e1_ship_uom_mult units,
             i.e1_whgt_uom_mult litres,
             NVL(sd.rr_rrp_oride,NVL(sd.shelf_price_sd,0)) ed_price,
             DECODE( pt.rr_margin_support , 1 , 0, sd.cust_input1)  ms_cd_not_ms_promo,  --- ms cd for non ms promo
             DECODE( pt.rr_margin_support , 1 , 0, sd.reg_raw_order)  ms_vol_not_ms_promo --- ms volume for non ms promo
--             sd.ebspricelist128 list_price_p, 
--             sd.ebspricelist123 cust_list_price_p, 
--             sd.ebspricelist100 tot_efficiencies_p, 
--             sd.ebspricelist124 market_price_p, 
--             sd.ebspricelist101 frieght_deduction_p, 
--             sd.ebspricelist125 bnps_p, 
--             sd.bdf_base_rate_corp bbds_p, 
--             sd.bdf_fixed_funds_corp rebate_p, 
--             sd.ebspricelist126 cogs_p, 
--             sd.ebspricelist102 distribution_p, 
--             sd.ebspricelist127 spoils_p, 
--             sd.ebs_sh_ship_qty_rd list_sales_a, 
--             sd.ebs_sh_req_qty_rd tot_efficiencies_a, 
--             sd.sdata12 price_adjust_a, 
--             sd.sdata10 gross_sales_a, 
--             sd.sdata11 base_non_per_spnd_a, 
--             sd.ebs_bh_req_qty_rd ttl_base_bus_dev_spnd_a, 
--             sd.ebs_sh_ship_qty_sd ttl_promo_trde_spnd_a, 
--             sd.sdata13 total_trad_a, 
--             sd.ebs_bh_book_qty_rd net_sales_a, 
--             sd.ebs_bh_req_qty_bd cogs_a, 
--             sd.ebs_bh_book_qty_bd tot_distribution_a, 
--             sd.sdata14 spoils_a, 
--             sd.sdata4 non_work_co_mark_a, 
--             sd.sdata9 cust_contribution_a,             
--             sd.ff_2 rr_roi_effeciencies,
--             sd.ff_4 rr_roi_rebate,
--             sd.ebspricelist129 def_efficiencies_rate_p,
--             sd.rev_diff bbds_deferred_p
             FROM sales_data sd, 
             promotion_type pt, 
             activity a,
             gondola_end ge,
             advertising adv,
             promotion_stat ps, 
             t_ep_l_att_6 latt6,
             mdp_matrix mdp,
             t_ep_item i
             WHERE 1 = 1
             AND mdp.item_id = sd.item_id
             AND mdp.location_id = sd.location_id
             AND mdp.t_ep_item_ep_id = i.t_ep_item_ep_id
             AND mdp.t_ep_l_att_6_ep_id = latt6.t_ep_l_att_6_ep_id
             AND pt.activity_id = a.activity_id
             AND pt.advertising_id = adv.advertising_id
             AND pt.gondola_end_id = ge.gondola_end_id
             AND ' || :inserted_promotion.activity_id || ' = a.activity_id
             AND ' || :inserted_promotion.advertising_id || ' = adv.advertising_id
             AND ' || :inserted_promotion.gondola_end_id || ' = ge.gondola_end_id   
             AND ps.promotion_stat_id = ' || :inserted_promotion.promotion_stat_id || '
             AND sd.item_id = ' || :inserted_promotion.item_id || '
             AND sd.location_id = ' || :inserted_promotion.location_id || '
             AND sd.sales_date = TO_DATE(''' || TO_CHAR(:inserted_promotion.sales_date, 'DD/MM/YYYY') || ''', ''DD/MM/YYYY'')'
      INTO V_VOLUME_BASE_TTL,V_VOL, V_EVT_VOL_ACT, V_INCR_EVT_VOL_ACT, 
      V_LIST_PRICE_P, V_CUST_LIST_PRICE_P, V_TOT_EFFICIENCIES_P, V_PRICE_ADJUST_A,V_BASE_NON_PER_SPND_A,
      V_MARKET_PRICE_P, V_TTL_BASE_BUS_DEV_SPND_A, V_FRIEGHT_DEDUCTION_P, V_BNPS_P, V_TTL_PROMO_TRDE_SPND_A, V_BBDS_P, 
      V_TOTAL_TRAD_A, V_REBATE_P, V_FRANCH_REIMB_P,V_COGS_P, V_UNITS, V_LITRES, v_ed_price,v_ms_cd_not_ms_promo,v_ms_vol_not_ms_promo ;
--      , v_distribution_p, v_spoils_p, 
--      v_list_sales_a, v_tot_efficiencies_a,  v_gross_sales_a,   
--       v_net_sales_a, v_cogs_a, v_tot_distribution_a, v_spoils_a, v_non_work_co_mark_a,
--      v_cust_contribution_a, v_units, v_rr_roi_effeciencies, v_rr_roi_rebate, v_def_efficiencies_rate_p, v_bbds_deferred_p;
      
      :INSERTED_PROMOTION.VOLUME_BASE_TTL := V_VOLUME_BASE_TTL;
      :inserted_promotion.rr_ms_vol := v_vol;
      :inserted_promotion.evt_vol_act := v_evt_vol_act;
      :inserted_promotion.incr_evt_vol_act := v_incr_evt_vol_act;
      :inserted_promotion.list_price_p := v_list_price_p;
      :inserted_promotion.list_price_pd := v_list_price_p;
      :inserted_promotion.cust_list_price_p := v_cust_list_price_p;
      :inserted_promotion.tot_efficiencies_p := v_tot_efficiencies_p;
      :inserted_promotion.market_price_p := v_market_price_p;
      :inserted_promotion.frieght_deduction_p := v_frieght_deduction_p;
      :inserted_promotion.bnps_p := v_bnps_p;
      :inserted_promotion.bbds_p := v_bbds_p;
      :inserted_promotion.rebate_p := v_rebate_p;
      :inserted_promotion.franch_reimb_p := v_franch_reimb_p;
      :inserted_promotion.cogs_p := v_cogs_p;
--      :inserted_promotion.distribution_p := v_distribution_p;
--      :inserted_promotion.spoils_p := v_spoils_p;
--      :inserted_promotion.bbds_deferred_p := v_bbds_deferred_p;
--      :inserted_promotion.list_sales_a := v_list_sales_a;
--      :inserted_promotion.tot_efficiencies_a := v_tot_efficiencies_a;
      :inserted_promotion.price_adjust_a := v_price_adjust_a;
--      :inserted_promotion.gross_sales_a := v_gross_sales_a;
      :inserted_promotion.base_non_per_spnd_a := v_base_non_per_spnd_a;
      :inserted_promotion.ttl_base_bus_dev_spnd_a := v_ttl_base_bus_dev_spnd_a;
      :inserted_promotion.ttl_promo_trde_spnd_a := v_ttl_promo_trde_spnd_a;
      :inserted_promotion.total_trad_a := v_total_trad_a;
--      :inserted_promotion.net_sales_a := v_net_sales_a;
--      :inserted_promotion.cogs_a := v_cogs_a;
--      :inserted_promotion.tot_distribution_a := v_tot_distribution_a;
--      :inserted_promotion.spoils_a := v_spoils_a;
--      :inserted_promotion.non_work_co_mark_a := v_non_work_co_mark_a;
--      :inserted_promotion.cust_contribution_a := v_cust_contribution_a;   
      :inserted_promotion.units := v_units;
      :inserted_promotion.litres := v_litres;
      :inserted_promotion.ed_price := v_ed_price;
      :inserted_promotion.ms_cd_not_ms_promo := v_ms_cd_not_ms_promo;
      :inserted_promotion.ms_vol_not_ms_promo := v_ms_vol_not_ms_promo;
--      :inserted_promotion.rr_roi_effeciencies := v_rr_roi_effeciencies;
--      :inserted_promotion.rr_roi_rebate := v_rr_roi_rebate;
--      :inserted_promotion.def_efficiencies_rate_p := v_def_efficiencies_rate_p;
      
   /*   :inserted_promotion.activity_type_id := :inserted_promotion.promotion_type_id;   */
 
EXCEPTION
   WHEN NO_DATA_FOUND
   THEN
      NULL;
END;
/
ALTER TRIGGER "DEMANTRA"."APPTRIG_INSERT_PROMOTION" ENABLE;
